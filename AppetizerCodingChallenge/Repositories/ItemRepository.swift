//
//  ItemRepository.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 09/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import Foundation
import RealmSwift

protocol ItemRepositoryInterface {
    func getItems(searchString: String, completionHandler: @escaping ((_ itemList: [Item]) -> Void))
}

struct ItemRepository: ItemRepositoryInterface {
    func getItems(searchString: String, completionHandler: @escaping ((_ itemList: [Item]) -> Void)) {
        guard !searchString.isEmpty else {
            let storedItems = getStoredItems()
            completionHandler(storedItems)
            return
        }
        
        guard let url = URL(string: "https://itunes.apple.com/search?") else {
            completionHandler([])
            return
        }
        let finalURL = url
            .appending("term", value: searchString)
            .appending("media", value: "movie")
            .appending("country", value: "AU")
        
        let request = URLRequest(url: finalURL)
        
        print("Requesting: \(request)")
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                completionHandler([])
                return
            }
            
            do {
                let decoded = try JSONDecoder().decode(SearchResult.self, from: data)
                self.store(items: decoded.results) // Save result to database
                completionHandler(decoded.results)
            } catch {
                assertionFailure("Decoding error: \(error.localizedDescription)")
            }
        }.resume()
    }
    
    private func getStoredItems() -> [Item] {
        let realm = try? Realm()
        return realm?.objects(RealmItem.self).map( { $0.map() } ) ?? []
    }
    
    private func store(items: [Item]) {
        let realm = try? Realm()
        let currentItems = realm?.objects(RealmItem.self)
        
        try? realm?.write {
            if let currentItems = currentItems {
                realm?.delete(currentItems)
            }
            
            realm?.add(items.map( { $0.map() } ))
        }
    }
}
