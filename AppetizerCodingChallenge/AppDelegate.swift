//
//  AppDelegate.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 09/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var coordinator: Coordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        // Instantiate a new UIWindow
        window = UIWindow(frame: UIScreen.main.bounds)
        
        // Instantiate the SplitViewController
        let splitVC = UISplitViewController()
        splitVC.view.backgroundColor = .gray
        splitVC.preferredDisplayMode = .allVisible
        
        // Instantiate the SearchTableViewController
        let searchVC = SearchTableViewController()
        let masterViewModel = MasterViewModel()
        searchVC.viewModel = masterViewModel
        let masterNav = UINavigationController(rootViewController: searchVC)
        
        // Instantiate the ItemDetailViewController
        let itemDetailViewController: ItemDetailViewController? = {
            let realm = try? Realm()
            guard let currentItem = realm?.objects(RealmCurrentItem.self).first else {
                return nil
            }
            return ItemDetailViewController(item: currentItem.map())
        }()
        let detailNav: UINavigationController? = {
            guard let detailViewController = itemDetailViewController else { return nil }
            return UINavigationController(rootViewController: detailViewController)
        }()
        
        // Assign viewControllers to the SplitViewController
        splitVC.viewControllers = [masterNav, detailNav].compactMap( { $0 } )
        
        // Instantiate the MainCoordinator
        if let primaryNav = splitVC.viewControllers.first as? UINavigationController {
            coordinator = MainCoordinator(navigationController: primaryNav)
            masterViewModel.coordinator = coordinator as? MainCoordinator
            itemDetailViewController?.coordinator = coordinator as? MainCoordinator
        }
        
        window?.rootViewController = splitVC
        window?.makeKeyAndVisible()
        
        // Log where realm file is stored
        print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first ?? "")
        
        return true
    }
}

