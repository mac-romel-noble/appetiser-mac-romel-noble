//
//  CustomImageView.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 10/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import UIKit

class CustomImageView: UIImageView {
    
    // Use this property to check if the image that was requested matches the result.
    // This is handy when reusing an image on a reusable tableViewCell
    private var urlStringKey: String?
    
    // Progress indicator while fetching an image from an external source
    private let spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: .gray)
        spinner.hidesWhenStopped = true
        return spinner
    }()
    
    init() {
        super.init(frame: CGRect.zero)
        
        addSubview(spinner)
        spinner.anchorCenterSuperview()
        
        contentMode = .scaleAspectFill
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func loadImageUsingUrlString(
        urlString: String,
        completion: ((_ isOk: Bool) -> Void)? = nil) {
        
        self.urlStringKey = urlString
        
        // Set placeholder image
        self.image = #imageLiteral(resourceName: "placeholder icon")
        
        if let cachedPhoto = imageCache.object(forKey: urlString as NSString) {
            image = cachedPhoto
        } else {
            getImageFromServer(urlString: urlString)
        }
    }
    
    private func getImageFromServer(urlString: String) {
        guard let url = URL(string: urlString) else { return }
        
        spinner.startAnimating()
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                self.spinner.stopAnimating()
                
                guard let data = data else { return }
                
                if let imageToCache = UIImage(data: data) {
                    imageCache.setObject(imageToCache, forKey: urlString as NSString)
                    
                    if self.urlStringKey == urlString {
                        self.image = imageToCache
                    }
                }
            }
        }.resume()
    }
}


