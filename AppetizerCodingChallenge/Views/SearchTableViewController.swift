//
//  MasterViewControllerV2.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 09/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SearchTableViewController: UITableViewController, Searchable {
    
    // ViewModel - must conform to these protocols
    var viewModel: (
        RowViewModelSource &
        DataReloadable &
        RowSelectable &
        SearchableViewModel)?
    
    // Private
    internal var searchController: UISearchController = UISearchController(searchResultsController: nil)
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        setupBindings()
        configureSearch()
    }
    
    private func setup() {
        title = "Search Movies"
        tableView.register(
            viewModel?.cellType().0 ?? UITableViewCell.self,
            forCellReuseIdentifier: viewModel?.cellType().cellReuseId ?? "")
        tableView.keyboardDismissMode = .onDrag
        
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    private func setupBindings() {
        viewModel?.reloadData = { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        // Search bar
        searchController.searchBar.rx.text.orEmpty
            .debounce(.milliseconds(500), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] (searchString) in
                self?.viewModel?.willSearch(with: searchString)
            }).disposed(by: disposeBag)
    }
}


// MARK: - TableView DataSource & Delegate
extension SearchTableViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.rowCount(section: section) ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: viewModel?.cellType().cellReuseId ?? "",
            for: indexPath)
        
        if let cell = cell as? CellConfigurable {
            if let rowViewModel = viewModel?.createViewModel(indexPath: indexPath) {
                cell.setup(viewModel: rowViewModel)
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.didSelectRow(at: indexPath)
    }
}
