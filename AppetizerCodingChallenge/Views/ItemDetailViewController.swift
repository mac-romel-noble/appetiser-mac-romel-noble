//
//  DetailViewController.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 09/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import UIKit

class ItemDetailViewController: UIViewController {
    
    // Coordinator
    weak var coordinator: ItemPresentable?
    
    // Model - can be an interface instead of a concrete object, but for simplicity this will do for now.
    private let item: Item
    
    // UI elements
    private let backgroundImageView = CustomImageView()
    private let topTextStack = StackFactory.createVerticalStack(spacing: 5)
    private let bottomTextStack = StackFactory.createVerticalStack(spacing: 5)
    private let mainStack = StackFactory.createVerticalStack(spacing: 15)
    private let titleLabel = UILabelFactory.createLabel(
        text: "Movie Title",
        color: .white,
        fontSize: 23,
        fontWeight: .heavy)
    private let subtitleLabel = UILabelFactory.createLabel(text: "Subtitle", color: UIColor.MyTheme.subtitleOnGray, fontSize: 15, fontWeight: nil)
    private let primaryButton = UIButton(type: .custom)
    private let readMoreButton = UIButton(type: .system)
    private let longDescLabel = UILabelFactory.createLabel(
        text: "Long description",
        color: .white,
        fontSize: 15,
        fontWeight: nil)
    
    // MARK: - Init
    init(item: Item) {
        self.item = item
        super.init(nibName: nil, bundle: nil)
        title = "Movie Details"
        
        configureView()
        
        setup()
    }
    
    private func setup() {
        navigationItem.largeTitleDisplayMode = .never
        backgroundImageView.addDarkBlurEffect()
        backgroundImageView.layer.masksToBounds = true
        view.addSubview(backgroundImageView)
        
        longDescLabel.numberOfLines = 3
        longDescLabel.lineBreakMode = .byTruncatingTail
        longDescLabel.adjustsFontSizeToFitWidth = false
        
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        subtitleLabel.textAlignment = .center
        
        topTextStack.addArrangedSubViews([titleLabel, subtitleLabel])
        bottomTextStack.addArrangedSubViews([longDescLabel, readMoreButton])
        
        mainStack.addArrangedSubViews([topTextStack, primaryButton, bottomTextStack])
        view.addSubview(mainStack)
        mainStack.alignment = .fill
        
        primaryButton.backgroundColor = UIColor.MyTheme.blue
        primaryButton.layer.cornerRadius = 10
        primaryButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .heavy)
        readMoreButton.setTitle("Read more", for: .normal)
        readMoreButton.addTarget(self, action: #selector(readMoreButtonHandler), for: .touchUpInside)
        readMoreButton.setTitleColor(UIColor.MyTheme.blueOnGray, for: .normal)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        backgroundImageView.fillSuperview()
        
        let safeArea = view.safeAreaLayoutGuide
        mainStack.anchor(
            nil,
            right: safeArea.rightAnchor,
            bottom: nil,
            left: safeArea.leftAnchor,
            topConstant:0,
            rightConstant: 20,
            bottomConstant: 0,
            leftConstant: 20,
            widthConstant: 0,
            heightConstant: 0)
        
        mainStack.anchorCenterYToSuperview()
        
        if UIDevice().screenType == .iPhones_5_5s_5c_SE {
            titleLabel.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        }
        
        // Set primary button constraints
        primaryButton.translatesAutoresizingMaskIntoConstraints = false
        primaryButton.heightAnchor.constraint(equalToConstant: 45).isActive = true
    }
    
    private func configureView() {
        // Update the user interface for the detail item.
        backgroundImageView.loadImageUsingUrlString(urlString: item.artworkUrl100 ?? "")
        
        titleLabel.text = item.trackName
        subtitleLabel.text = item.subtitle
        primaryButton.setTitle("Buy $ " + String(item.trackPrice ?? 0), for: .normal)
        
        longDescLabel.attributedText = item.longDescription?.attributedString(withLineSpacing: 4)
    }
    
    @objc
    func readMoreButtonHandler() {
        coordinator?.willPresentItem(item)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

