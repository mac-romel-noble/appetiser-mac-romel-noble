//
//  TestCell.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 09/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import UIKit

protocol RowViewModel { }

protocol CellConfigurable {
    func setup(viewModel: RowViewModel)
}

protocol CustomCellViewModel: RowViewModel {
    var textLabelList: [UILabel] { get }
    var thumbURLString: String? { get }
}

class CustomCell: UITableViewCell {
    
    var viewModel: CustomCellViewModel? {
        didSet {
            guard let viewModel = viewModel else { return }
            textStack.addArrangedSubViews(viewModel.textLabelList)
            photoView.loadImageUsingUrlString(urlString: viewModel.thumbURLString ?? "")
        }
    }
    
    // UI elements
    private let photoView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.backgroundColor = .green
        imageView.layer.cornerRadius = 10
        imageView.layer.masksToBounds = true
        return imageView
    }()
    private let mainStack = StackFactory.createStack(
        axis: .horizontal,
        spacing: 10,
        alignment: .top,
        distribution: .fill)
    private let textStack = StackFactory.createStack(
        axis: .vertical,
        spacing: 3,
        alignment: .leading,
        distribution: .fill)

    // MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        contentView.addSubview(mainStack)
        
        mainStack.addArrangedSubViews([photoView, textStack])
        
        mainStack.anchor(
            contentView.topAnchor,
            right: contentView.rightAnchor,
            bottom: contentView.bottomAnchor,
            left: contentView.leftAnchor,
            topConstant: 13,
            rightConstant: 15,
            bottomConstant: 15,
            leftConstant: 15,
            widthConstant: 0,
            heightConstant: 0)
        
        photoView.translatesAutoresizingMaskIntoConstraints = false
        let photoHeightConstraint = photoView.heightAnchor.constraint(equalToConstant: 70)
        photoHeightConstraint.priority = UILayoutPriority(rawValue: 999)
        photoHeightConstraint.isActive = true
        photoView.widthAnchor.constraint(equalToConstant: 70).isActive = true
        photoView.setContentHuggingPriority(.required, for: .horizontal)
        photoView.setContentCompressionResistancePriority(.required, for: .horizontal)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        textStack.removeAllArrangedSubViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - CellConfigurable
extension CustomCell: CellConfigurable {
    func setup(viewModel: RowViewModel) {
        self.viewModel = viewModel as? CustomCellViewModel
    }
}
