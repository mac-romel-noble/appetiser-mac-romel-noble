//
//  TableViewProtocols.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 09/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import Foundation

protocol DataReloadable {
    var reloadData: (() -> Void)? { get set }
    var notifyLoading: ((_ isLoading: Bool) -> Void)? { get set }
    var isLoading: Bool { get set }
}

protocol RowViewModelSource {
    var sectionCount: Int { get }
    func rowCount(section: Int) -> Int
    func cellType() -> (AnyClass?, cellReuseId: String)
    func createViewModel(indexPath: IndexPath) -> RowViewModel
}

protocol RowSelectable {
    func didSelectRow(at indexPath: IndexPath)
}
