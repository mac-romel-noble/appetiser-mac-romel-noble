//
//  NetworkActivityIndicator.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 10/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import UIKit

struct NetworkActivityIndicator {
    static func start() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    static func stop() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
