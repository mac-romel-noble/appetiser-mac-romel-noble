//
//  UIStackViewExtension.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 09/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import UIKit

extension UIStackView {
    func addArrangedSubViews(_ view: [UIView]) {
        view.forEach( { addArrangedSubview($0) } )
    }
    
    func removeAllArrangedSubViews() {
        arrangedSubviews.forEach( { $0.removeFromSuperview() } )
    }
}
