//
//  ColorTheme.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 09/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import UIKit

extension UIColor {
    struct MyTheme {
        static var blue: UIColor {
            return UIColor(red:0.00, green:0.57, blue:1.00, alpha:1.0)
        }
        
        static var subTitle1: UIColor {
            return UIColor(red:0.56, green:0.56, blue:0.58, alpha:1.0)
        }
        
        static var subtitleOnGray: UIColor {
            return UIColor(red:0.76, green:0.76, blue:0.78, alpha:1.0)
        }
        
        static var blueOnGray: UIColor {
            return UIColor(red:0.06, green:0.78, blue:1.00, alpha:1.0)
        }
    }
}
