//
//  UILabelFactory.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 09/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import UIKit

struct UILabelFactory {
    static func createLabel(text: String?, color: UIColor?, fontSize: CGFloat?, fontWeight: UIFont.Weight?) -> UILabel {
        let label = UILabel()
        label.text = text
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.8
        if let color = color {
            label.textColor = color
        }
        if let fontSize = fontSize {
            label.font = label.font.withSize(fontSize)
            label.setContentCompressionResistancePriority(.required, for: .vertical)
        }
        if let fontWeight = fontWeight {
            label.font = UIFont.systemFont(ofSize: fontSize ?? UIFont.labelFontSize, weight: fontWeight)
        }
        return label
    }
}
