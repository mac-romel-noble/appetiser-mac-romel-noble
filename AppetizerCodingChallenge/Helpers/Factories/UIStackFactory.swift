//
//  UIStackFactory.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 09/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import UIKit

struct StackFactory {
    
    // For horizontal stacks
    static func createHorizontalStack() -> UIStackView {
        return createStack(axis: .horizontal, spacing: 0, alignment: .fill, distribution: .fill)
    }
    
    static func createHorizontalStack(spacing: CGFloat) -> UIStackView {
        return createStack(axis: .horizontal, spacing: spacing, alignment: .fill, distribution: .fill)
    }
    
    // For vertical stacks
    static func createVerticalStack() -> UIStackView {
        return createStack(axis: .vertical, spacing: 0, alignment: .fill, distribution: .fill)
    }
    
    static func createVerticalStack(spacing: CGFloat) -> UIStackView  {
        return createStack(axis: .vertical, spacing: spacing, alignment: .fill, distribution: .fill)
    }
    
    static func createStack(axis: NSLayoutConstraint.Axis, spacing: CGFloat, alignment: UIStackView.Alignment?, distribution: UIStackView.Distribution?) -> UIStackView  {
        let stack = UIStackView()
        stack.axis = axis
        stack.alignment = alignment ?? .fill
        stack.spacing = spacing
        stack.distribution = distribution ?? .fill
        return stack
    }
}
