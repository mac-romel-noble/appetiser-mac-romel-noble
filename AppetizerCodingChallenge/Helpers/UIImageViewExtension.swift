//
//  UIImageExtension.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 10/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import UIKit

extension UIImageView {
    func addDarkBlurEffect() {
        let darkVisualEffect = UIBlurEffect(style: .dark)
        let visualEffectView = UIVisualEffectView(effect: darkVisualEffect)
        
        addSubview(visualEffectView)
        visualEffectView.fillSuperview()
    }
}
