//
//  StringExtension.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 10/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import UIKit

extension String {
    func attributedString(withLineSpacing lineSpacing: CGFloat = 2) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        
        let paragraphStyle = NSMutableParagraphStyle()

        // Set LineSpacing property in points
        paragraphStyle.lineSpacing = lineSpacing // Line spacing in points

        // Apply attribute to string
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))

        return attributedString
    }
    
    // Formats a string to a date string for display
    func getFormattedDateString(withFormat format: String = "yyyy") -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        guard let date = dateFormatter.date(from: self) else {
            return nil
        }
        
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
}
