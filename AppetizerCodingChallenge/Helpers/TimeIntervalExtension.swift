//
//  TimeIntervalExtension.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 10/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import Foundation

extension TimeInterval {
    // Used to get hours and minutes, for display
    func stringFromTimeInterval() -> String {
        let time = NSInteger(self)

        let minutes = (time / 60) % 60
        let hours = (time / 3600)

        return String(format: "%0.2d hr %0.2d min", hours, minutes)
    }
}
