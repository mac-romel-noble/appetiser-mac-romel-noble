//
//  RealmItem.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 10/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import Foundation
import RealmSwift

class RealmItem: Object {
    @objc dynamic var trackName: String = ""
    var trackPrice = RealmOptional<Double>()
    @objc dynamic var artworkUrl60: String? = nil
    @objc dynamic var artworkUrl100: String? = nil
    @objc dynamic var primaryGenreName: String? = nil
    @objc dynamic var releaseDate: String = ""
    var trackTimeMillis = RealmOptional<Int>() // milliseconds
    @objc dynamic var longDescription: String? = nil
    
    func map() -> Item {
        let item = Item(
            trackName: trackName,
            trackPrice: trackPrice.value,
            artworkUrl60: artworkUrl60,
            artworkUrl100: artworkUrl100,
            primaryGenreName: primaryGenreName,
            releaseDate: releaseDate,
            trackTimeMillis: trackTimeMillis.value,
            longDescription: longDescription)
        return item
    }
}

class RealmCurrentItem: RealmItem { }
