//
//  ItemCellViewModel.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 10/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import UIKit

struct ItemCellViewModel: CustomCellViewModel {
    
    var thumbURLString: String?
    var textLabelList: [UILabel] = []
    
    init(item: Item) {
        thumbURLString = item.artworkUrl100
        
        let titleLabel = UILabelFactory.createLabel(text: item.trackName, color: nil, fontSize: 17, fontWeight: nil)
        titleLabel.numberOfLines = 2
        textLabelList.append(titleLabel)
        
        if let price = item.trackPrice {
            let priceLabel = UILabelFactory.createLabel(text: "$ " + String(price), color: UIColor.MyTheme.blue, fontSize: 14, fontWeight: nil)
            textLabelList.append(priceLabel)
        }
        
        let subtitleLabel = UILabelFactory.createLabel(text: item.subtitle, color: UIColor.MyTheme.subTitle1, fontSize: 12, fontWeight: nil)
        textLabelList.append(subtitleLabel)
    }
}
