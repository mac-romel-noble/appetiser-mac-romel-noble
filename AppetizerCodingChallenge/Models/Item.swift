//
//  Item.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 09/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import Foundation
import RealmSwift

struct SearchResult: Codable {
    let resultCount: Int
    let results: [Item]
    
    private enum CodingKeys: String, CodingKey {
        case resultCount = "resultCount"
        case results = "results"
    }
}

struct Item: Codable {
    let trackName: String
    let trackPrice: Double?
    let artworkUrl60: String?
    let artworkUrl100: String?
    let primaryGenreName: String?
    let releaseDate: String
    let trackTimeMillis: Int? // milliseconds
    let longDescription: String?
    
    // A string from the combination of genre, date, & duration
    var subtitle: String {
        get {
            let trackDuration: String? = {
                if let trackTimeMillis = trackTimeMillis {
                    let timeInterval = TimeInterval(trackTimeMillis / 1000)
                    return timeInterval.stringFromTimeInterval()
                }
                return nil
            }()
            
            return [primaryGenreName, releaseDate.getFormattedDateString(), trackDuration]
                .compactMap({ $0 })
                .joined(separator: " · ")
        }
    }
    
    private enum CodingKeys: String, CodingKey {
        case trackName = "trackName"
        case trackPrice = "trackPrice"
        case artworkUrl60 = "artworkUrl60"
        case artworkUrl100 = "artworkUrl100"
        case primaryGenreName = "primaryGenreName"
        case releaseDate = "releaseDate"
        case trackTimeMillis = "trackTimeMillis"
        case longDescription = "longDescription"
    }
    
    func map() -> RealmItem {
        let realmItem = RealmItem()
        realmItem.trackName = trackName
        realmItem.trackPrice = RealmOptional<Double>.init(trackPrice)
        realmItem.artworkUrl60 = artworkUrl60
        realmItem.artworkUrl100 = artworkUrl100
        realmItem.primaryGenreName = primaryGenreName
        realmItem.releaseDate = releaseDate
        realmItem.trackTimeMillis = RealmOptional<Int>.init(trackTimeMillis)
        realmItem.longDescription = longDescription
        return realmItem
    }
}
