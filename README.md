# Appetiser - Mac Romel Noble

Hello, this is a sample app created for the coding challenge of Appetiser

![picture](https://bitbucket.org/mac-romel-noble/appetiser-mac-romel-noble/raw/5aa2737b1b67815027baf1385c1c0e5998ce3905/Images/Appetiser%20mockup.jpg)

## Architecture
### MVVM
* Makes it easier to collaborate with a team. Developers can work on views and business logic separately.
* Views will be more reusable since they are unaware of models
* More easy to test & mock

### Coordinator
* Used to manage the navigation flow of the app
* Reduces coupling of viewControllers

### Composite reuse principle
* Use of composition over inheritance. Abundant use of protocols.

## Persistence
Uses Realm as the persistence mechanism
### Persisted data
* Current item that was viewed
* Last search results

## More features
* Image caching
* Device orientation - Portrait and Landscape
* SplitView - On Plus devices, split screen is displayed on landscape mode
* Search throttling
```
searchController.searchBar.rx.text.orEmpty
    // Wait for half a second before sending a request. Avoids sending multiple request while typing.
    .debounce(.milliseconds(500), scheduler: MainScheduler.instance)
    // Check if the new search term is distinct
    .distinctUntilChanged()
    .subscribe(onNext: { [weak self] (searchString) in
        self?.viewModel?.willSearch(with: searchString)
    }).disposed(by: disposeBag)
```
* Dynamic layout - iPhone 5s support
